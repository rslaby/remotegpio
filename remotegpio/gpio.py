from RPi import GPIO
import socket

pins = [5, 6, 7, 8, 9, 
       10, 11, 12, 13, 16,
       17, 18, 19, 20, 21, 
       22, 23, 24, 25, 26, 
       27]

HOST = "0.0.0.0"
PORT = 65432


def gpioinit():
    GPIO.setmode(GPIO.BCM)
    for pin in pins:
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, GPIO.LOW)


def gpiohigh(pin):
    GPIO.output(pin, GPIO.HIGH)


def gpiolow(pin):
    GPIO.output(pin, GPIO.LOW)


if __name__ == "__main__":
    GPIO.setmode(GPIO.BCM)
    print("Available Cues: %i" % len(pins))
    for pin in pins:
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, GPIO.LOW)
    
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen()
        conn, addr = s.accept()
        with conn:
            print(f"Connected by {addr}")
            while True:
                data = conn.recv(1024)
                if not data:
                    break
                conn.sendall(data)
