from flask import Flask, jsonify, render_template, request
from remotegpio import gpio
import time

app = Flask(__name__)

pins = { '1' : 11,
         '2' : 5,
         '3' : 6,
         '4' : 13,
         '5' : 27,
         '6' : 22,
         '7' : 10,
         '8' : 9,
         '9' : 16,
        '10' : 12,
        '11' : 20,
        '12' : 21,
        '13' : 8,
        '14' : 25,
        '15' : 23,
        '16' : 24 }


print(gpio.pins)

gpio.gpioinit()


@app.route("/", methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        if request.form.get('action1'):
            pin = request.form.get('action1').split()[1]
            print(f'Fire {pin}')
            gpio.gpiohigh(pins[pin])
            time.sleep(2)
            gpio.gpiolow(pins[pin])
        else:
            print('Nada')
    elif request.method == 'GET':
        return render_template('index.html', pins=pins)

    return render_template('index.html', pins=pins)
